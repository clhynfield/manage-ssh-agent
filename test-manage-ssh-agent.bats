#!/usr/bin/env bats

setup() {
  load 'manage-ssh-agent'
}

@test "Starts ssh-agent" {
  run start_ssh_agent bash -c 'exec' '3>&-'
  echo "$output" | sed 's/^/# /g' >&3
  eval "$( echo \"$output\" | grep 'PID=' )"
  ps "$SSH_AGENT_PID" | sed 's/^/# /g' >&3
  ps "$SSH_AGENT_PID"
}

