#!/usr/bin/env bash

set -e

: "${AGENT_SH:=$HOME/.ssh/agent.sh}"

start_ssh_agent() {
  ssh-agent "$@"
}

if [[ -f "$AGENT_SH" ]]; then
  source "$AGENT_SH"
else
  start_ssh_agent > "$AGENT_SH"
fi
